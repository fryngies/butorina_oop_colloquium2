// БСШ – класс list: пример использования – заданы 2 упорядоченных списка фамилий, слить их в один общий упорядоченный список.

#include <iostream>
#include <list>
#include <string>

using namespace std;

int main() {
	list<string> l;

	l.push_back("Ye");
	l.push_back("Ya");
	l.push_back("Qua");

	l.sort();

	for(list<string>::iterator it = l.begin(); it != l.end(); ++it)
		cout << *it << " ";
	cout << endl;

	return 0;
}

