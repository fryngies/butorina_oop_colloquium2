// БСШ – класс vector: пример использования – задать массив чисел Фибоначчи, выбрать из него в другой массив четные числа.

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

bool is_simple(int val) {
	if(val == 1 || val == 2 || val == 3)
		return true;

	for(int i = 2; i <= val/2; ++i)
		if(!(val % i))
			return false;
	return true;
}

int main() {
	const int V_ORIG_LENGTH = 10;

	vector<int> v_orig;
	vector<int> v_new;

	v_orig.push_back(1);
	cout << v_orig.back() << " ";
	v_orig.push_back(1);
	cout << v_orig.back() << " ";
	for(int i = 2; i < V_ORIG_LENGTH; ++i) {
		v_orig.push_back(v_orig[i-1] + v_orig[i-2]);
		cout << v_orig[i] << " ";
	}
	cout << endl;

	for(int i = 0; i < V_ORIG_LENGTH; ++i)
		if(is_simple(v_orig[i])) {
			v_new.push_back(v_orig[i]);
			cout << v_orig[i] << " ";
		}
	cout << endl;

	return 0;
}

