#include <iostream>
#include <list>
#include <string>

using namespace std;

int main() {
	list<string> l;
	list<string>::iterator res;
	string fam = "wah";
	int max_len = 0;

	for(int i = 0; i < 10; ++i) {
		string buff;

		cin >> buff;
		l.push_back(buff);
	}

	for(list<string>::iterator it = l.begin(); it != l.end(); ++it) {
		int buff = it->length();

		if(buff > max_len) {
			max_len = buff;
			res = it;
		}
	}

	res->insert(0, fam);
	cout << *res << endl;

	return 0;
}

