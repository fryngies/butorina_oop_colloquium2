// Класс BM (булева матрица): конструкторы, операции &, &=, |, |=, ~, ==, !=, сдвиги <<, >>, ввод, вывод, найти вес  i-й строки, установить в 1 j-ю компоненту i-й строки, сбросить в 0 i j-ю компоненту i-й строки, инвертировать j-ю компоненту i-й строки, установить в 1 p компонент, начиная с i, сбросить в 0 p компонент, начиная с i, инвертировать p компонент, начиная с i, потоковый ввод /вывод.

#include "BV.cpp"

class BM
{
	BV *bm; int n, m;
public:
	BM(BM&);

	BV operator[] (int k);
	BV operator& ();
	BM operator& (const BM &);
	BM operator&= (const BM &);
	BM operator| (const BM &);
	BM operator|= (const BM &);
	BM operator~ ();
	bool operator== (const BM &);
	bool operator!= (const BM &);
	BM operator<< (int k);
	BM operator>> (int k);

	friend std::ostream& operator <<(std::ostream &r, BM &a);
	friend std::istream& operator >>(std::istream &r, BM &a);
};

BM::BM(BM& x)
{
	m=x.m; n=x.n;
	bm = new BV[m];
	for(int i =0; i<m; i++)
		bm[i] = x.bm[i];
}

BV BM::operator[] (int k)
{
	return bm[k];
}

BV BM::operator& ()
{
	BV r(n);
	r.in_1(0, n-1);
	for(int i = 0; i<m; i++)
		r &= bm[i];
	return r;
}

BM BM::operator& (BM& x)
{
	BM r(m,n);
	for(int i = 0; i<m; i++)
		r.bm[i] = bm[i] & x.bm[i];
	return r;
}

BM operator&= (const BM & x)
{
	for(int i = 0; i<m; i++)
		bm[i] &= x.bm[i];
	return *this;
}

BM BM::operator| (BM& x)
{
	BM r(m,n);
	for(int i = 0; i<m; i++)
		r.bm[i] = bm[i] | x.bm[i];
	return r;
}

BM BM::operator|= (const BM & x)
{
	for(int i = 0; i<m; i++)
		bm[i] |= x.bm[i];
	return *this;
}

BM BM::operator~ ()
{
	BM r(*this);
	for(int i = 0; i<r.m; i++)
		r.bm[i] = ~r.bm[i];
	return *this;
}

bool BM::operator== (const BM & x)
{
	bool flag = true;
	for (int i = 0; i < m; ++i)
		if (x[i] != bm[i])
			flag = false;
	return flag;
}

bool BM::operator!= (const BM & x)
{
	return !((*this) == x);
}

BM BM::operator<< (int k)
{
	BM r(m, n);
	for (int i = 0; i < m; ++i)
		r[i] <<= k;
	return r;
}

BM BM::operator>> (int k)
{
	BM r(m, n);
	for (int i = 0; i < m; ++i)
		r[i] >>= k;
	return r;
}
