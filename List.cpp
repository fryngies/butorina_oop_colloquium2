// Класс Список: конструкторы, деструктор, перегрузка операций >> (потоковый ввод), << (потоковый вывод), = (присвоение), добавление элемента в голову, в хвост, на позицию, после ключа, удаление элемента из головы, из хвоста, по позиции, по ключу, функция поиска

#include <iostream>

using namespace std;

class List;

class Node
{
	int n;
	Node* next = nullptr;
	Node* prev = nullptr;

	friend class List;
	friend istream & operator >>(istream& r, Node& l);
	friend ostream & operator <<(ostream& r, Node& l);
	friend ostream & operator <<(ostream& r, List& l);
	friend istream & operator >>(istream& r, List& l);

public:
	Node() : n(0) {}
	Node(int k) : n(k) {}
	int& key() {return n;}
};

class List
{
	Node *beg, *end;
public:
	List()
	{
		beg = new Node, end = new Node;
		beg->next = end;
		end->prev = beg;
	}
	List(int*, int);
	List(List &l);
	~List();

	bool isEmpty() { return beg->next == end; }
	Node* find(int k);

	List& addOnPosition(int, int);
	List& addAfterKey(int, int);
	List& delOnPosition(int);
	List& delOnKey(int);

	List& operator =(List&);
	Node& operator [](int k);
	List& operator +=(int); // добавление элемента в хвост
	List& operator +(int); // добавление элемента в голову
	List& operator -(int); // удаление элемента из головы
	List& operator -=(int); // удаление элемента из хвоста

	friend istream & operator >>(istream& r, List& l);
	friend ostream & operator <<(ostream& r, List& l); 
};


List::List(int* arr, int len)
{
	beg = new Node, end = new Node;
	beg->next = end;
	end->prev = beg;
	for (int i = 0; i < len; ++i)
		*this += arr[i];
}

List::List(List &l)
{
	beg = new Node, end = new Node;
	beg->next = end;
	end->prev = beg;
	for (Node* p = l.beg->next; p != l.end; p = p->next)
		*this += p->key();
}

List::~List()
{
	Node* buf;
	for (Node* p = beg->next; p != end; p = p->next, delete buf)
		buf = p;
}

Node* List::find(int k)
{
	Node* p = beg;
	while(p->key() != k && p != end)
		p = p->next;
	return p != end ? p : nullptr;
}

List& List::addOnPosition(int n, int el)
{
	Node* buf = new Node(el);

	buf->next = (*this)[k]->next;
	buf->prev = (*this)[k];
	(*this)[k]->next = buf;
	buf->next->prev = buf;

	return *this;
}

List& List::addAfterKey(int key, int el)
{
	Node* buf = new Node(el);
	Node* pos_el = find(key);

	if(pos_el == nullptr)
		throw "Key not found!"

	buf->next = pos_el->next;
	buf->prev = pos_el;
	pos_el->next = buf;
	buf->next->prev = buf;
	
	return *this;
}

List& List::delOnPosition(int k)
{
	Node* buff = (*this)[k];
	buff->next->prev = buff->prev;
	buff->prev->next = buff->next;
	delete buff;
	return *this;
}

List& List::delOnKey(int k)
{
	Node* buff = find(k);
	buff->next->prev = buff->prev;
	buff->prev->next = buff->next;
	delete buff;
	return *this;	
}

List& List::operator =(List &l)
{
	if(this == &l)
		return *this;
	this->~List();
	List* new_list = new List(l);
	beg = new_list->beg;
	end = new_list->end;
	return *this;
}

Node& List::operator [](int k)
{
	Node* p = beg->next;
	for (int i = 0; i < k; ++i)
		p = p == end ? beg->next : p->next;
	return *p;
}

List& List::operator +=(int k)
{
	Node* p = new Node(k);
	p->next = end;
	p->prev = end->prev;
	p->prev->next = p;
	end->prev = p;
	return *this;
}

List& List::operator +(int k)
{
	List* l = new List(*this);
	l += k;
	return *this;
}

List& List::operator -(int k)
{
	List* l = new List(*this);

	Node* buff = l->beg->next;
	buff->next->prev = l->beg;
	l->beg->next = buff->next;
	delete buff;
	
	return *l;
}

List& List::operator -=(int k)
{
	Node* buff = end->prev;
	end->prev = buff->prev;
	buff->prev->next = end;
	delete buff;
	
	return *l;
}

istream & operator >>(istream& r, Node& l)
{
	return (r >> l.n);
}

ostream & operator <<(ostream& r, Node& l)
{
	return (r << l.n);
}

istream & operator >>(istream& r, List& l)
{
	int k;
	for (Node* p = l.beg->next; p != l.end; p = p->next)
	{
		r >> k;
		l += k;
	}
	return r;
}

ostream & operator <<(ostream& r, List& l)
{
	for (Node* p = l.beg->next; p != l.end; p = p->next)
		r << *p << " ";
	return r;
}
