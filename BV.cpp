#include <sstream>
#include <iostream>
#include <cmath>
#include <sstream>

#define UC unsigned char

class BV
{
	UC* bv;
	int dlina, mem;// mem кол-во байт
	int fill(int, int, char*, int);
public:
	BV(int _mem = 1);
	BV(const char*);
	BV(const char*, int);
	BV(BV &v);
	~BV()
	{
		if (bv)
		{
			delete[] bv; 
			bv = 0; 
		}
	};
	void in_1(int);
	void in_0(int);
	void enter();
	void print();
	void oneinversion(int i);
	void inversion(int start);
	void in_1_start(int start);
	int Weight();

	//перегрузки
	int operator[](int);
	BV operator&(BV&a);
	BV operator&=(BV&a);
	BV operator<<(int i);
	BV operator>>(int i);
	BV operator^(BV&v);
	BV operator^=(BV&v);
	BV operator|=(BV&v);
	BV operator>>=(int);
	BV operator<<=(int i);
	BV operator|(BV &v);
	BV operator=(BV &v);
	BV operator~();
	friend std::ostream& operator <<(std::ostream &r, BV &a);
	friend std::istream& operator >>(std::istream&r, BV &a);
};

inline BV::BV(int _mem)
{
	bv = new UC[mem = ((dlina = _mem) - 1) / 8 + 1]{0};
}

int BV::fill(int i_bv, int kol, char*s, int i_s)
{
	for (int i = 0; s[i_s] && i < kol; i++, i_s++)
	{
		bv[i_bv] << 1;
		if (s[i_s] == 1)
			bv[i_bv]++;
	}
	return i_s;
}

BV::BV(const char*s)
{
	bv = new UC[mem = ((dlina = strlen(s)) - 1) / 8 + 1]{0};
	for (int i = 0; i < dlina; i++)
		if (s[i] == '1')
			in_1(i);
}

BV::BV(const char *s, int n)
{
	int i;
	bv = new UC[mem = ((dlina = n) - 1) / 8 + 1]{0};
	for (i = 0; (unsigned int)i < (strlen(s) < (unsigned int)dlina ? strlen(s) : dlina); ++i)
	if (s[i] == '1')
		in_1(i);
}

BV::BV(BV &v)
{
	dlina = v.dlina;
	bv = new UC[mem = v.mem];
	for (int i = 0; i < mem; i++)
	{
		bv[i] = v.bv[i];
	}
}

void BV::in_1(int i)
{
	int k = i / 8;
	bv[k] = bv[k] | (1 << (i % 8));
}

void BV::in_0(int i)
{
	int k = i / 8;
	bv[k] = bv[k] & ~(1 << (i % 8));
}

int BV::Weight()
{
	int k = 0;
	for (int i = 0; i < dlina; i++)
		k += (*this)[i];
	return k;
}

void BV::enter()
{
	std::cout << "Введите количество:" << std::endl;
	std::cin >> dlina;
	bv = new UC[mem = dlina / 8 + 1]{0};
	int *mas = new int[dlina];
	std::cout << "Введите вектор: " << std::endl;
	for (int i = 0; i < dlina; i++)
	{
		std::cin >> mas[i];
	}
	for (int i = 0; i < dlina; i++)
	{
		if (mas[i] == 1)
			in_1(i);
	}
	delete[] mas;
}

void BV::print()
{
	std::cout << "Вывод: ";
	for (int i = 0; i < (*this).dlina; i++)
		std::cout << (*this)[i]<<" ";
	std::cout << std::endl;
}

//инверсия одного элемента
void BV::oneinversion(int i)
{
	if ((*this)[i] == 1)
		in_0(i);
	else
	if ((*this)[i] == 0)
		in_1(i);
}

// инверсия указаных компонент начиная с указаного 
void BV::inversion(int start)
{
	for (int i = start; i < dlina; i++)
	{
		(*this).oneinversion(i);
	}
}

//установка 1 с указаной компоненты
void BV::in_1_start(int start)
{
	for (int i = start; i < (*this).dlina; i++)
	{
		if ((*this)[i] == 0)
			in_1(i);
	}
}

int BV::operator[](int i)
{
	int k = i / 8;
	return (bv[k] & (1 << (i % 8))) ? 1 : 0;
}

BV BV::operator~()
{
	BV* g = new BV(*this);

	for (int i = 0; i < g->mem; ++i)
		g->bv[i] = ~g->bv[i];

	if (g->dlina % 8)
	{
		UC mask = (1 << (g->dlina % 8)) - 1;
		g->bv[mem + 1] = g->bv[mem - 1] & mask;
	}

	return *g;
}

BV BV::operator^(BV &v)
{
	BV* g = dlina > v.dlina ? this : new BV(v);

	for (int i = 0; i < (mem > v.mem ? mem : v.mem); ++i)
		g->bv[i] = bv[i] ^ v.bv[i];

	return *g;
}

BV BV::operator&(BV&v)
{
	BV* g = dlina > v.dlina ? this : new BV(v);

	for (int i = 0; i < (mem > v.mem ? mem : v.mem); ++i)
		g->bv[i] = bv[i] & v.bv[i];

	return *g;
}

BV BV::operator&=(BV&a)
{
	*this = *this&a;
	return *this;
}

BV BV::operator<<(int i)
{
	BV* rez = new BV(dlina);
	for (int q = 0; q < (dlina - i); q++)
	if ((*this)[i + q])
		rez->in_1(q);
	else
		rez->in_0(q);
	for (int q = dlina - i; q < dlina; q++)
		rez->in_0(q);
	return *rez;
}

BV BV::operator>>(int k)
{
	BV* v = new BV(dlina);

	for (int i = k; i < dlina; ++i)
	if ((*this)[i - k])
		v->in_1(i);
	else
		v->in_0(i);

	for (int i = 0; i < k; ++i)
		v->in_0(i);

	return *v;
}

BV BV::operator|(BV &v)
{
	BV* g = dlina > v.dlina ? this : new BV(v);
	for (int i = 0; i < (mem > v.mem ? mem : v.mem); ++i)
		g->bv[i] = bv[i] | v.bv[i];

	return *g;
}

BV BV::operator|=(BV &v)
{
	*this = *this | v;
	return *this;
}

BV BV::operator^=(BV &v)
{
	*this = *this^v;
	return *this;
}

BV BV::operator=(BV &v)
{
	if (this == &v) return *this;
	if (bv) delete[]bv;

	dlina = v.dlina;
	bv = new UC[mem = v.mem];

	for (int i = 0; i < mem; ++i)
		bv[i] = v.bv[i];

	return *this;
}

BV BV::operator<<=(int i)
{
	*this = *this << i;
	return *this;
}

BV BV::operator>>=(int i)
{
	*this = *this >> i;
	return *this;
}

std::ostream& operator <<(std::ostream &r, BV &a)
{
	std::cout << "Работает потоковый вывод" << std::endl;
	for (int i = 0; i < a.dlina; i++)
		std::cout << a[i]<<" ";
	std::cout << std::endl;
	return r;
}

std::istream& operator >>(std::istream&r, BV &a)
{
	a.enter();
	return r;
}