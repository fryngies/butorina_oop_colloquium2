// БСШ – класс set:пример использования – заданы 2 предложения. В каком предложении разных букв больше?

#include <iostream>
#include <set>

using namespace std;

int main() {
	string s1 = "blabla";
	string s2 = "blable";

	set<char> set1;
	set<char> set2;

	for(string::iterator it = s1.begin(); it != s1.end(); ++it)
		set1.insert(*it);
	for(string::iterator it = s2.begin(); it != s2.end(); ++it)
		set2.insert(*it);

	cout << (set1.size() < set2.size()) << endl;

	return 0;
}

