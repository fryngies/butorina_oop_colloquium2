#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

bool is_simple(int val) {
	if(val == 1 || val == 2 || val == 3)
		return true;

	for(int i = 2; i <= val/2; ++i)
		if(!(val % i))
			return false;
	return true;
}

int main() {
	const int V_ORIG_LENGTH = 10;

	vector<int> v_orig;
	vector<int> v_new;

	for(int i = 0; i < V_ORIG_LENGTH; ++i) {
		v_orig.push_back(rand() % 30);
		cout << v_orig[i] << " ";
	}
	cout << endl;

	for(int i = 0; i < V_ORIG_LENGTH; ++i)
		if(is_simple(v_orig[i])) {
			v_new.push_back(v_orig[i]);
			cout << v_orig[i] << " ";
		}
	cout << endl;

	return 0;
}

