#include <vector>
#include <set>
#include <iostream>

using namespace std;

int main() {
	vector<int> v;
	set<int> s;
	int n;

	cin >> n; 

	for (int i = 0; i < n; i++) {
		v.push_back(rand() % 30); // adds element at the end
		cout << v[i] << " ";
	}
	cout << endl;

	for (int i = 0; i < n; i++)
		s.insert(v[i]); // insert element

	for(set<int>::iterator ps = s.begin(); ps != s.end(); ps++)
		cout << *ps << " ";
	cout << endl;

	return 0;
}

