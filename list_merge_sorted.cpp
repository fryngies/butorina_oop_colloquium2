// БСШ – класс list: пример использования – заданы 2 упорядоченных списка фамилий, слить их в один общий упорядоченный список.

#include <iostream>
#include <list>
#include <string>

using namespace std;

int main() {
	list<string> l1;
	list<string> l2;
	list<string> nl;

	l1.push_back("Bla");
	l1.push_back("Ble");
	l1.sort();

	l2.push_back("Baa");
	l2.push_back("Blsdfsadf");
	l2.sort();

	nl = l1;
	nl.merge(l2);

	for(list<string>::iterator it = nl.begin(); it != nl.end(); ++it)
		cout << *it << " ";
	cout << endl;

	return 0;
}

