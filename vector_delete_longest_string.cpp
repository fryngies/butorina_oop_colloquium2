// БСШ – класс vector: пример использования – задан массив строк. Удалить самую длинную строку.

#include <iostream>
#include <vector>

using namespace std;

int main() {
	vector<string> v;
	vector<string>::iterator res;
	int max_len = 0;

	for(int i = 0; i < 10; ++i) {
		string buff;

		cin >> buff;
		v.push_back(buff);
	}

	for(vector<string>::iterator it = v.begin(); it != v.end(); ++it) {
		int buff = it->length();

		if(buff > max_len) {
			max_len = buff;
			res = it;
		}
	}

	v.erase(res);
	for(vector<string>::iterator it = v.begin(); it != v.end(); ++it)
		cout << *it << " ";
	cout << endl;

	return 0;
}

